FROM node:15-alpine
WORKDIR "/app"
COPY package.json .
RUN yarn install --silent
COPY . .
RUN yarn run build
RUN yarn global add serve
EXPOSE 8080
CMD ["serve", "-l", "8080", "-s", "build"]
