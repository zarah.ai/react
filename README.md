### Available Commands

* `yarn start` - Runs the app in the development mode.
* `yarn build` - Builds the app for production to the `build` folder.


### Deploy docker image

```
sudo docker run -d --name react --restart unless-stopped \
--label=com.centurylinklabs.watchtower.enable=true \
-p 442:8080 registry.gitlab.com/zarah.ai/react

```
