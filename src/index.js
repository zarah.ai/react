import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import firebase from "firebase/app";
import "firebase/analytics";
import "firebase/performance";

const firebaseConfig = {
  apiKey: "AIzaSyDIuS3_YpUjB2QEgY-rJpK6WOH9CRiwQPc",
  authDomain: "zarah-f9e57.firebaseapp.com",
  projectId: "zarah-f9e57",
  storageBucket: "zarah-f9e57.appspot.com",
  messagingSenderId: "983312132919",
  appId: "1:983312132919:web:eccc4a4b114ed7e00b88b2",
  measurementId: "G-VE7MLDXWBP"
};

firebase.initializeApp(firebaseConfig);

firebase.analytics();
firebase.performance();

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
