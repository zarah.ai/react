import React from "react";
import { Link } from "react-router-dom";
import icon from '../resources/icon.svg';

export default class Header extends React.Component {

  render() {
    if (window.location.search.includes("noheader")) return null;
    return (
      <div className="semantic header">
        <Link to="/">
          <img className="header-home-image" src={icon} alt="zarah.ai" width="48" height="48"/>
          <span className="header-home-title">zarah.ai</span>
        </Link>
        <Link to="/chat?noheader&nofooter" target="_blank">
          <span className="header-chat-title">Chat with Zarah (beta)</span>
        </Link>
      </div>
    );
  }

}
