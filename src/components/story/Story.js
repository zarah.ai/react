import React from "react";
import { Link } from "react-router-dom";
import "./Story.css"
import creationofai from '../../resources/creationofai.jpg';
import landscape from '../../resources/landscape.jpg';
import chat1 from '../../resources/chat1.jpg';
import chat2 from '../../resources/chat2.jpg';
import sonar from '../../resources/sonar.svg';
import gitlab from '../../resources/gitlab.svg';
import Slider from "react-slick";
import "./Slick.css"

export default class Story extends React.Component {

  render() {
    return (
      <div className="content-main">
        <img className="content-main-image" src={creationofai} alt="Creation of AI" width="100%"/>
        <div className="content-main-about">
          <span className="content-main-heading">Your Personal Safe Space</span>
          <span className="content-main-subheading">What if you could have someone to talk to, whatever the time of day?</span>
          <img className="content-main-image content-main-chat" src={chat1} alt="Chat example" width="45%" />
          <img className="content-main-image content-main-chat" src={chat2} alt="Chat example" width="45%" />
        </div>
        <div className="content-main-believe">
          <div className="content-main-believe-block">
              <img className="content-main-image" src={landscape} alt="Landscape" width="100%" />
              <div className="content-main-believe-background">
                <span className="content-main-heading content-main-believe-heading">Transparency</span>
                <span className="content-main-subheading content-main-believe-subheading">We believe in openness and transparency, that's why we make our source code available.</span>
                <div className="content-main-believe-links">
                  <a href="https://gitlab.com/zarah.ai" target="_blank" className="content-main-believe-link">
                    <img className="content-main-image content-main-believe-link-image" src={gitlab} alt="Gitlab" width="24px" />
                    <span className="content-main-believe-link-text">Gitlab</span>
                  </a>
                  <a href="https://sonarcloud.io/organizations/zarah/projects" target="_blank" className="content-main-believe-link">
                    <img className="content-main-image content-main-believe-link-image" src={sonar} alt="Sonar" width="24px" />
                    <span className="content-main-believe-link-text">Sonar</span>
                  </a>
                </div>
              </div>
          </div>
        </div>
        <div className="content-main-testimonials">
          <span className="content-main-heading">Don't just take our word for it</span>
          <span className="content-main-subheading">Thousands of users went before you, what are you waiting for?</span>
          <Slider className="conent-main-testimonial-carousel" dots={true} infinite={true} autoplay={true} autoplaySpeed={5000} pauseOnHover={true}>
            <div className="conent-main-testimonial-carousel-item">
              <span className="conent-main-testimonial-carousel-item-text">"Arcu risus quis varius quam quisque id diam. Etiam tempor orci eu lobortis elementum nibh. Nunc sed velit dignissim sodales ut eu sem integer. Egestas egestas fringilla phasellus faucibus scelerisque eleifend. Quam lacus suspendisse faucibus interdum. Venenatis a condimentum vitae sapien pellentesque habitant morbi. Fusce id velit ut tortor pretium viverra suspendisse potenti."</span>
              <span className="conent-main-testimonial-carousel-item-name">Sed Blandit, 28</span>
            </div>
            <div className="conent-main-testimonial-carousel-item">
              <span className="conent-main-testimonial-carousel-item-text">"Gravida dictum fusce ut placerat orci nulla pellentesque. Blandit aliquam etiam erat velit scelerisque in dictum non. Fermentum odio eu feugiat pretium nibh. Placerat duis ultricies lacus sed turpis tincidunt. Dui id ornare arcu odio ut sem. Dictum sit amet justo donec enim. Eros in cursus turpis massa tincidunt dui ut ornare."</span>
              <span className="conent-main-testimonial-carousel-item-name">Morbi Tristique, 39</span>
            </div>
            <div className="conent-main-testimonial-carousel-item">
              <span className="conent-main-testimonial-carousel-item-text">"Ornare suspendisse sed nisi lacus sed viverra tellus. Pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus. Augue interdum velit euismod in pellentesque massa placerat duis ultricies. Elementum sagittis vitae et leo. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Sit amet volutpat consequat mauris."</span>
              <span className="conent-main-testimonial-carousel-item-name">Vestibulum Rhoncus, 72</span>
            </div>
            <div className="conent-main-testimonial-carousel-item">
              <span className="conent-main-testimonial-carousel-item-text">"Venenatis a condimentum vitae sapien pellentesque habitant morbi. Fusce id velit ut tortor pretium viverra suspendisse potenti. Sit amet est placerat in. Pulvinar etiam non quam lacus suspendisse faucibus interdum. Purus gravida quis blandit turpis. Suscipit tellus mauris a diam maecenas sed. Arcu odio ut sem nulla pharetra diam sit. Mi proin sed libero enim sed."</span>
              <span className="conent-main-testimonial-carousel-item-name">Malesuada Proin, 48</span>
            </div>
            <div className="conent-main-testimonial-carousel-item">
              <span className="conent-main-testimonial-carousel-item-text">"Commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend. Cursus turpis massa tincidunt dui ut ornare lectus sit. Amet consectetur adipiscing elit ut. In metus vulputate eu scelerisque felis. Interdum velit euismod in pellentesque massa placerat duis. Id neque aliquam vestibulum morbi blandit cursus risus."</span>
              <span className="conent-main-testimonial-carousel-item-name">Bibendum Arcu, 17</span>
            </div>
          </Slider>
        </div>
      </div>
    );
  }
}
