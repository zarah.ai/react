import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Chat from "./chat/Chat";
import Story from "./story/Story";
import Header from "./Header";
import Footer from "./Footer";
import Terms from "./legal/Terms";
import Privacy from "./legal/Privacy";
import "./App.css"

export default class App extends React.Component {

  render() {
    return (
      <BrowserRouter forceRefresh={true}>
        <Header />
        <div className="content">
          <Switch>
            <Route path="/legal/privacy"><Privacy /></Route>
            <Route path="/legal/terms"><Terms /></Route>
            <Route path="/chat"><Chat /></Route>
            <Route path="/story"><Story /></Route>
            <Route path="/"><Story /></Route>
          </Switch>
        </div>
        <Footer />
      </BrowserRouter>
    );
  }
}
