import React from "react";
import { Link } from "react-router-dom";
import apple from '../resources/apple.svg';
import android from '../resources/android.svg';


export default class Footer extends React.Component {

  render() {
    if (window.location.search.includes("nofooter")) return null;
    return (
      <div className="semantic footer">
        <div className="footer-links">
          <div className="footer-links-about">
            <span className="footer-links-about-title">About zarah.ai</span>
            <Link to="/story" className="footer-links-about-button">
              <span className="footer-links-about-text">Our story</span>
            </Link>
            <a target="_blank" href="mailto:contact@zarah.ai" className="footer-links-about-button">
              <span className="footer-links-about-text">Contact us</span>
            </a>
          </div>
          <br className="footer-links-linebreak"/>
          <br className="footer-links-linebreak"/>
          <div className="footer-links-legal">
          <span className="footer-links-legal-title">Legal</span>
            <Link to="/legal/terms" className="footer-links-legal-button">
              <span className="footer-links-legal-text">Terms of Service</span>
            </Link>
            <Link to="/legal/privacy" className="footer-links-legal-button">
              <span className="footer-links-legal-text">Privacy Policy</span>
            </Link>
          </div>
          <br className="footer-links-linebreak"/>
          <br className="footer-links-linebreak"/>
          <div className="footer-links-get">
            <span className="footer-links-get-title">Get the app</span>
            <a target="_blank" href="https://itunes.apple.com/id1556999144" className="footer-links-get-button">
              <img className="footer-links-get-img" src={apple} alt="Apple" width="18" height="18"/>
              <span className="footer-links-get-text">iOS</span>
            </a>
            <a target="_blank" href="https://play.google.com/store/apps/details?id=ai.zarah.android" className="footer-links-get-button">
              <img className="footer-links-get-img" src={android} alt="Android" width="18" height="18"/>
              <span className="footer-links-get-text">Android</span>
            </a>
          </div>
        </div>
        <span className="footer-copyright">Copyright © 2021 zarah.ai. All Rights Reserved.</span>
      </div>
    );
  }

}
