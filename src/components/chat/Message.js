import React, { Component } from "react";
import './Message.css';

export default class Message extends Component {

  render() {
    const d = new Date(this.props.chat.timestamp);
    const t = d.getHours() + ":" + d.getMinutes().toString().padStart(2, "0");
    const c = this.props.chat.sender === "Me" ? "chat-bubble chat-me" : "chat-bubble chat-them";
    return (
      <div className="chat">
        <div className={c}>
          <span className="chat-text">{this.props.chat.message}</span>
          <span className="chat-time">{t}</span>
        </div>
      </div>
    );
  }
}
