import Popup from "./Popup";
import Message from "./Message";
import { v4 as uuid } from 'uuid';
import './Chat.css';
import zarah from '../../resources/zarah.jpg';
import more from '../../resources/more.svg';
import send from '../../resources/send.svg';
import React from "react";

export default class Chat extends React.Component {
  state = {
   showPopup: false,
   messages: 0,
   online: false,
   chat: [{ message: "Hi! I'm Zarah. I'm not real but sometimes I pretend to be", sender:"Zarah", timestamp: Date.now(), id: uuid()  }]
  };

  moreClicked = () => {
    this.setState({ showPopup: !this.state.showPopup });
  };

  sendMessage = (e) => {
    e.preventDefault();

    if (!this.input.value) {
      return
    }

    const c = { message: this.input.value, sender:"Me", timestamp: Date.now(), id: uuid() };
    this.setState({ chat: this.state.chat.concat(c), messages: this.state.messages+1 });

    this.input.value = "";

    const request = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(c)
    };
    fetch("https://zarah.ai/api/chat", request)
    .then(r => r.json())
    .then(d => this.setState({ chat: this.state.chat.concat(d) }))
    .catch(console.log);
    console.log(this.state.chat);
  }

  componentDidMount() {
    fetch("https://zarah.ai/api/status")
    .then(r => r.json())
    .then((d) => { this.setState({ messages: d.messageCount, online: true }) })
    .catch(console.log);
  }

  componentDidUpdate() {
    this.bodyEnd.scrollIntoView({ behavior: 'smooth' });
  }

  render() {
    return (
      <div className="chat-wrap">
        <div className="chat-content">
          <div className="chat-header">
            <img className="zarah" src={zarah} alt="Zarah" width="64" height="64"/>
            <div className="title">
              <span className="zarah-title">Chat with Zarah</span>
              <br />
              <span className="chat-title">{this.state.messages} messages</span>
            </div>
            <button className="more-button" onClick={this.moreClicked}>
              <img className="more-image" src={more} alt="More" width="32" height="32"/>
            </button>
            {this.state.online ? <div className="online online-green" /> : <div className="online online-red" />}
          </div>
          {this.state.showPopup ? <Popup toggle={this.moreClicked} /> : null}
          <div className="chat-body">
          {this.state.chat.map(c => <Message chat={c} key={c.id} />)}
          <div ref={(r) => this.bodyEnd = r} />
          </div>
          <div className="chat-footer">
            <form className="text-form" onSubmit={this.sendMessage}>
              <input className="text-input" type="text" placeholder="Send a message..." ref={(r) => this.input = r} />
              <button type="submit" className="send-button">
                <img className="send-image" src={send} alt="Send" width="32" height="32"/>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
