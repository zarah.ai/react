import React, { Component } from "react";
import './Popup.css';
import close from '../../resources/close.svg';

export default class Popup extends Component {
  render() {
    return (
      <div className="modal">
        <div className="modal-content">
          <button className="close-button" onClick={this.props.toggle}>
            <img className="close-image" src={close} alt="Close" width="18" height="18"/>
          </button>
          <br/>
          <p className="modal-text">Zarah is a conversational agent based on <a href="https://github.com/ywk991112/pytorch-chatbot" target="_blank">Yuan-Kuei Wu’s pytorch-chatbot implementation</a>.</p>
          <p className="copyright-text">Copyright © 2021 zarah.ai<br />All Rights Reserved.</p>
        </div>
      </div>
    );
  }
}
