import React from "react";


export default class Terms extends React.Component {

  render() {
    return (
      <div className="legal-content">
        <h1>Terms &amp; Conditions</h1>
        <h3>Last updated: March 7th, 2021</h3>
        <p>By downloading or using the website, app or other zarah.ai software (after: the App), these terms will automatically apply to you – you should make sure therefore that you read them carefully before using the App. While our code is open source, you are not allowed to use or exploit our trademarks in any way. The software itself, and all the trademarks, copyright, database rights and other intellectual property rights related to it, still belong to zarah.ai.</p>
        <p>zarah.ai is committed to ensuring that the App is as useful and efficient as possible. For that reason, we reserve the right to make changes to the App or to charge for its services, at any time and for any reason. We will never charge you for the App or its services without making it very clear to you exactly what you’re paying for.</p>
        <p>zarah.ai stores and processes personal data that you have provided to us, in order to improve the App. It’s your responsibility to keep your pc, phone, laptop and internet connection secure. We therefore recommend that you do not jailbreak or root your device, which is the process of removing software restrictions and limitations imposed by the official operating system of your device. It could make your phone vulnerable to malware/viruses/malicious programs, compromise your device’s security features.</p>
        <p>You should be aware that there are certain things that zarah.ai will not take responsibility for. Certain functions of the App will require an active internet connection. The connection can be Wi-Fi, or provided by your mobile network provider, but zarah.ai cannot take responsibility for the App not working at full functionality if you don’t have access to Wi-Fi, and you don’t have any of your data allowance left.</p>
        <p>If you’re using the App outside of an area with Wi-Fi, you should remember that your terms of the agreement with your mobile network provider will still apply. As a result, you may be charged by your mobile provider for the cost of data for the duration of the connection while accessing the App. In using the App, you’re accepting responsibility for any such charges, including roaming data charges if you use the App outside of your home territory (i.e. region or country) without turning off data roaming. If you are not the bill payer for the device on which you’re using the app, please be aware that we assume that you have received permission from the bill payer for using the App.</p>
        <p>Along the same lines, zarah.ai cannot always take responsibility for the way you use the App i.e. You need to make sure that your device stays charged – if it runs out of battery and you can’t turn it on to avail the App, zarah.ai cannot accept responsibility</p>
        <p>With respect to zarah.ai’s responsibility for your use of the App, when you’re using the App, it’s important to bear in mind that although we endeavour to ensure that it is updated and correct at all times, we do rely on third parties to provide information to us so that we can make it available to you. zarah.ai accepts no liability for any loss, direct or indirect, you experience as a result of relying wholly on this functionality of the App.</p>
        <p>At some point, we may wish to update the App. The App is currently available on Android, iOS and on the web – the requirements for these systems (and for any additional systems we decide to extend the availability of the App to) may change, and you’ll need to download the updates if you want to keep using the App. zarah.ai does not promise that it will always update the App so that it is relevant to you and/or works with the operating system version that you have installed on your device. However, you promise to always accept updates to the App when offered to you, We may also wish to stop providing the App, and may terminate use of it at any time without giving notice of termination to you. Unless we tell you otherwise, upon any termination, (a) the rights and licenses granted to you in these terms will end; (b) you must stop using the App, and (if needed) delete it from your device.</p>
        <h2>Changes to This Terms and Conditions</h2>
        <p>We may update our Terms and Conditions from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Terms and Conditions on this page. These changes are effective immediately after they are posted on this page.</p>
        <h2>Contact Us</h2>
        <p>If you have any questions or suggestions about our Terms and Conditions, do not hesitate to <a href="mailto:contact@zarah.ai">contact us</a>.</p>
      </div>
    );
  }

}
